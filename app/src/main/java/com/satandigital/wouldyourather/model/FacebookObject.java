package com.satandigital.wouldyourather.model;

/**
 * Project : WouldYouRather
 * Created by Sanat Dutta on 10/3/2016.
 * http://www.satandigital.com/
 */

public class FacebookObject {

    private String facebookEmail = "NA";
    private String facebookUserId = "NA";
    private String facebookUserName = "NA";
    private String facebookAgeRange = "NA";
    private String facebookLink = "NA";
    private String facebookGender = "NA";
    private String facebookLocale = "NA";
    private String facebookDynamicPicture = "NA";
    private String facebookCDNPicture = "NA";
    private String facebookTimeZone = "NA";
    private String facebookUpdatedTime = "NA";
    private String facebookVerified = "NA";

    public String getFacebookEmail() {
        return facebookEmail;
    }

    public void setFacebookEmail(String facebookEmail) {
        this.facebookEmail = facebookEmail;
    }

    public String getFacebookUserId() {
        return facebookUserId;
    }

    public void setFacebookUserId(String facebookUserId) {
        this.facebookUserId = facebookUserId;
    }

    public String getFacebookUserName() {
        return facebookUserName;
    }

    public void setFacebookUserName(String facebookUserName) {
        this.facebookUserName = facebookUserName;
    }

    public String getFacebookAgeRange() {
        return facebookAgeRange;
    }

    public void setFacebookAgeRange(String facebookAgeRange) {
        this.facebookAgeRange = facebookAgeRange;
    }

    public String getFacebookLink() {
        return facebookLink;
    }

    public void setFacebookLink(String facebookLink) {
        this.facebookLink = facebookLink;
    }

    public String getFacebookGender() {
        return facebookGender;
    }

    public void setFacebookGender(String facebookGender) {
        this.facebookGender = facebookGender;
    }

    public String getFacebookLocale() {
        return facebookLocale;
    }

    public void setFacebookLocale(String facebookLocale) {
        this.facebookLocale = facebookLocale;
    }

    public String getFacebookDynamicPicture() {
        return facebookDynamicPicture;
    }

    public void setFacebookDynamicPicture(String facebookDynamicPicture) {
        this.facebookDynamicPicture = facebookDynamicPicture;
    }

    public String getFacebookCDNPicture() {
        return facebookCDNPicture;
    }

    public void setFacebookCDNPicture(String facebookCDNPicture) {
        this.facebookCDNPicture = facebookCDNPicture;
    }

    public String getFacebookTimeZone() {
        return facebookTimeZone;
    }

    public void setFacebookTimeZone(String facebookTimeZone) {
        this.facebookTimeZone = facebookTimeZone;
    }

    public String getFacebookUpdatedTime() {
        return facebookUpdatedTime;
    }

    public void setFacebookUpdatedTime(String facebookUpdatedTime) {
        this.facebookUpdatedTime = facebookUpdatedTime;
    }

    public String getFacebookVerified() {
        return facebookVerified;
    }

    public void setFacebookVerified(String facebookVerified) {
        this.facebookVerified = facebookVerified;
    }
}

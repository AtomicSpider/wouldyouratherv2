package com.satandigital.wouldyourather.model;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

/**
 * Project : WouldYouRather
 * Created by Sanat Dutta on 10/6/2016.
 * http://www.satandigital.com/
 */

public class FirebaseUserClass {

    public String userName = "";
    public String displayName = "";
    public String email = "";
    public String photoUrl = "";

    public FirebaseUserClass() {
    }

    public FirebaseUserClass(String userName, String displayName, String email, String photoUrl) {
        this.userName = userName;
        this.displayName = displayName;
        this.email = email;
        this.photoUrl = photoUrl;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("username", userName);
        result.put("display_name", displayName);
        result.put("email", email);
        result.put("photo_url", photoUrl);

        return result;
    }
}

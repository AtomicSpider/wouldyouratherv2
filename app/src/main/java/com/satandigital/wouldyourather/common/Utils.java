package com.satandigital.wouldyourather.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.satandigital.wouldyourather.WouldYouRatherApp;
import com.satandigital.wouldyourather.model.FacebookObject;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Project : WouldYouRather
 * Created by Sanat Dutta on 10/3/2016.
 * http://www.satandigital.com/
 */

public class Utils {

    private static final String TAG = Utils.class.getSimpleName();

    public static boolean isUserValid() {
        return FirebaseAuth.getInstance().getCurrentUser() != null;
    }

    public static FirebaseUser getCurrentUser() {
        return FirebaseAuth.getInstance().getCurrentUser();
    }

    public static DatabaseReference getRef() {
        return FirebaseDatabase.getInstance().getReference();
    }

    public static DatabaseReference getUsernamesRef() {
        return FirebaseDatabase.getInstance().getReference("usernames");
    }

    public static void saveToSharedPreferences(String key, String sValue) {
        SharedPreferences.Editor mEditor = WouldYouRatherApp.mSharedPreferences.edit();
        mEditor.putString(key, sValue);
        mEditor.apply();
    }

    public static String readSharedPreferences(String key, String valueDefault) {
        return WouldYouRatherApp.mSharedPreferences.getString(key, valueDefault);
    }

    public static void hideKeyboard(Context c, IBinder windowToken) {
        InputMethodManager imm = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(windowToken, 0);
    }

    public static FacebookObject generateFbObject(JSONObject object) {
        FacebookObject mFacebookObject = new FacebookObject();

        try {
            String minAge = "0", maxAge = "0";

            //ToDo remove
            Log.d(TAG, "" + object);

            if (object.has("email"))
                mFacebookObject.setFacebookEmail(object.getString("email"));
            if (object.has("id"))
                mFacebookObject.setFacebookUserId(object.getString("id"));
            if (object.has("name"))
                mFacebookObject.setFacebookUserName(object.getString("name"));
            if (object.getJSONObject("age_range").has("min"))
                minAge = object.getJSONObject("age_range").getString("min");
            if (object.getJSONObject("age_range").has("max"))
                maxAge = object.getJSONObject("age_range").getString("max");
            mFacebookObject.setFacebookAgeRange(minAge + "-" + maxAge);
            if (object.has("link"))
                mFacebookObject.setFacebookLink(object.getString("link"));
            if (object.has("gender"))
                mFacebookObject.setFacebookGender(object.getString("gender"));
            if (object.has("locale"))
                mFacebookObject.setFacebookLocale(object.getString("locale"));
            if (object.has("id"))
                mFacebookObject.setFacebookDynamicPicture("http://graph.facebook.com/" + mFacebookObject.getFacebookUserId() + "/picture?type=large");
            if (object.getJSONObject("picture").has("data")) {
                if (object.getJSONObject("picture").getJSONObject("data").has("url"))
                    mFacebookObject.setFacebookCDNPicture(object.getJSONObject("picture").getJSONObject("data").getString("url"));
            }
            if (object.has("timezone"))
                mFacebookObject.setFacebookTimeZone(object.getString("timezone"));
            if (object.has("updated_time"))
                mFacebookObject.setFacebookUpdatedTime(object.getString("updated_time"));
            if (object.has("verified"))
                mFacebookObject.setFacebookVerified(object.getString("verified"));
        } catch (JSONException e) {
            Log.e(TAG, "Error decoding Facebook Data");
            return null;
        }

        return mFacebookObject;
    }
}

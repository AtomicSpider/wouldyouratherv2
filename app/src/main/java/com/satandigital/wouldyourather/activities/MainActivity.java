package com.satandigital.wouldyourather.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.satandigital.wouldyourather.BuildConfig;
import com.satandigital.wouldyourather.R;
import com.satandigital.wouldyourather.auth.AuthUI;
import com.satandigital.wouldyourather.common.Utils;
import com.satandigital.wouldyourather.model.FirebaseUserClass;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.satandigital.wouldyourather.auth.ui.AcquireEmailHelper.RC_SIGN_IN;

/**
 * Project : Stocks
 * Created by Sanat Dutta on 10/3/2016.
 * http://www.satandigital.com/
 */

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private final String TAG = MainActivity.class.getSimpleName();

    //Views
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.fab)
    FloatingActionButton mFAB;
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    private Menu mNavMenu;
    private ProgressDialog mProgressDialog;
    private View positiveAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setUpToolbar();
        setUpFAB();
        setUpNavigationDrawer();

        validateNavigationDrawer();
        if (Utils.isUserValid()) startUserActiveTasks();
    }

    private void startUserActiveTasks() {
        Log.d(TAG, "User active");
        startUserNameSetTask();
    }

    private void startUserNameSetTask() {
        Utils.getRef().child("users").child(Utils.getCurrentUser().getUid()).child("username").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) showSetUsernameDialog("");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "getUser:onCancelled: " + databaseError.getMessage(), databaseError.toException());
            }
        });
    }

    private void showSetUsernameDialog(final String errorMessage) {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title(getString(R.string.set_username))
                .customView(R.layout.dialog_username, true)
                .positiveText("Submit")
                .negativeText("Cancel")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        EditText userNameInput = (EditText) dialog.getCustomView().findViewById(R.id.username);
                        saveUsernameToFirebase(userNameInput.getText().toString());
                    }
                }).build();

        positiveAction = dialog.getActionButton(DialogAction.POSITIVE);
        EditText userNameInput = (EditText) dialog.getCustomView().findViewById(R.id.username);
        final TextView errorTV = (TextView) dialog.getCustomView().findViewById(R.id.errorText);
        if (!errorMessage.equals("")) {
            errorTV.setVisibility(View.VISIBLE);
            errorTV.setText(errorMessage);
        }

        userNameInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!errorMessage.equals("")) errorTV.setVisibility(View.GONE);
                if (s.toString().length() > 0 && s.toString().length() < 16)
                    positiveAction.setEnabled(true);
                else positiveAction.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        dialog.show();
        positiveAction.setEnabled(false);
    }

    private void saveUsernameToFirebase(String username) {
        Uri photoUri = Utils.getCurrentUser().getPhotoUrl();
        String photoUrl = "";
        if (photoUri != null) photoUrl = photoUri.toString();
        FirebaseUserClass mFirebaseUserClass = new FirebaseUserClass(username.toLowerCase(), username, Utils.getCurrentUser().getEmail(), photoUrl);
        Map<String, Object> mFirebaseUserValues = mFirebaseUserClass.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/users/" + Utils.getCurrentUser().getUid(), mFirebaseUserValues);
        childUpdates.put("/usernames/" + username.toLowerCase(), Utils.getCurrentUser().getUid());

        mProgressDialog = ProgressDialog.show(MainActivity.this, "", getString(R.string.pls_wait), true);
        Utils.getRef().updateChildren(childUpdates).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                mProgressDialog.dismiss();
                Log.d(TAG, "Username change failure " + e.getMessage());
                if (e.getMessage().equals("Firebase Database error: Permission denied"))
                    showSetUsernameDialog(getString(R.string.username_taken));
                else showSetUsernameDialog(getString(R.string.username_dialog_error));
            }
        }).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                mProgressDialog.dismiss();
                Log.d(TAG, "Username change success");
                Toast.makeText(MainActivity.this, "Username set.", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setUpToolbar() {
        setSupportActionBar(mToolbar);
    }

    private void setUpFAB() {
        mFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }

    private void setUpNavigationDrawer() {
        ActionBarDrawerToggle mToggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();

        NavigationView mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavMenu = mNavigationView.getMenu();
        mNavigationView.setNavigationItemSelectedListener(this);
    }

    private void validateNavigationDrawer() {
        boolean userActive = Utils.isUserValid();
        mNavMenu.findItem(R.id.nav_sign_in).setVisible(!userActive);
        mNavMenu.findItem(R.id.nav_sign_out).setVisible(userActive);
        mNavMenu.findItem(R.id.nav_user_profile).setVisible(userActive);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        switch (id) {
            case R.id.nav_sign_in:
                signInFromNav();
                break;
            case R.id.nav_sign_out:
                signOutFromNav();
                break;
        }

        return true;
    }

    private void signOutFromNav() {
        mProgressDialog = ProgressDialog.show(MainActivity.this, "", getString(R.string.signing_out), true);
        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    public void onComplete(@NonNull Task<Void> task) {
                        mProgressDialog.dismiss();
                        validateNavigationDrawer();
                        Toast.makeText(MainActivity.this, "User signed out", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void signInFromNav() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() != null) {
            Toast.makeText(MainActivity.this, "User already exists", Toast.LENGTH_LONG).show();
        } else {
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setProviders(
                                    AuthUI.EMAIL_PROVIDER,
                                    AuthUI.GOOGLE_PROVIDER,
                                    AuthUI.FACEBOOK_PROVIDER)
                            .setIsSmartLockEnabled(!BuildConfig.DEBUG)
                            .setTosUrl("https://www.google.com/")
                            .build(),
                    RC_SIGN_IN);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {

            validateNavigationDrawer();

            if (resultCode == RESULT_OK) {
                startUserActiveTasks();
            } else {
                Toast.makeText(MainActivity.this, "Sign in cancelled", Toast.LENGTH_LONG).show();
            }
        }
    }
}

package com.satandigital.wouldyourather;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.facebook.FacebookSdk;
import com.google.firebase.database.FirebaseDatabase;
import com.satandigital.wouldyourather.common.AppCodes;

/**
 * Project : WouldYouRather
 * Created by Sanat Dutta on 10/3/2016.
 * http://www.satandigital.com/
 */

public class WouldYouRatherApp extends Application {

    private String TAG = WouldYouRatherApp.class.getSimpleName();

    //Data
    public static SharedPreferences mSharedPreferences;

    @Override
    public void onCreate() {
        super.onCreate();

        mSharedPreferences = getSharedPreferences(AppCodes.prefName, Context.MODE_PRIVATE);

        FacebookSdk.sdkInitialize(getApplicationContext());
    }
}
